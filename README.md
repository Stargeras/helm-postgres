# Pull down chart from oci registry
```bash
helm registry login registry.gitlab.com
helm pull oci://registry.gitlab.com/stargeras/helm-postgres/postgres --version=1.0.0
```

# Sample terraform code to deploy from OCI registry
```terraform
provider "helm" {
  registry {
    url      = "oci://registry.gitlab.com"
    username = "username"
    password = "password"
  }
}

resource "helm_release" "postgres" {
  name             = "postgres"
  namespace        = "postgres"
  repository       = "oci://registry.gitlab.com"
  version          = "1.0.0"
  chart            = "stargeras/helm-postgres/postgres"
  create_namespace = true
}
```

