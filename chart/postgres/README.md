# postgres

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| http://apache.github.io/superset/ | superset | x.x.x |
| https://charts.bitnami.com/bitnami | postgresql | x.x.x |
| oci://registry.gitlab.com/stargeras/helm-pgadmin | pgadmin | 1.0.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| fullnameOverride | string | `""` |  |
| global.ingress.className | string | `"nginx"` |  |
| global.ingress.domain | string | `"example.com"` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"nginx"` |  |
| image.tag | string | `""` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.tls | list | `[]` |  |
| livenessProbe.httpGet.path | string | `"/"` |  |
| livenessProbe.httpGet.port | string | `"http"` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| pgadmin.enabled | bool | `true` |  |
| pgadmin.ingress.className | string | `"{{ .Values.global.ingress.className }}"` |  |
| pgadmin.ingress.enabled | bool | `true` |  |
| pgadmin.ingress.hosts[0].host | string | `"pgadmin.{{ .Values.global.ingress.domain }}"` |  |
| pgadmin.ingress.hosts[0].paths[0].path | string | `"/"` |  |
| pgadmin.ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| pgadmin.ingress.tls[0].hosts[0] | string | `"pgadmin.{{ .Values.global.ingress.domain }}"` |  |
| pgadmin.postgresConnection.enabled | bool | `true` |  |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| postgresql.auth.database | string | `"admin"` |  |
| postgresql.auth.existingSecret | string | `"postgres-credentials"` |  |
| postgresql.auth.password | string | `"password"` |  |
| postgresql.auth.username | string | `"admin"` |  |
| postgresql.primary.initdb.scripts."init-dbs.sql" | string | `"CREATE DATABASE superset WITH OWNER admin;\nGRANT ALL PRIVILEGES ON DATABASE superset TO admin;"` |  |
| readinessProbe.httpGet.path | string | `"/"` |  |
| readinessProbe.httpGet.port | string | `"http"` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.port | int | `80` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.automount | bool | `true` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `""` |  |
| superset.customIngress.className | string | `"{{ .Values.global.ingress.className }}"` |  |
| superset.customIngress.enabled | bool | `true` |  |
| superset.customIngress.hosts[0].host | string | `"superset.{{ .Values.global.ingress.domain }}"` |  |
| superset.customIngress.hosts[0].paths[0].path | string | `"/"` |  |
| superset.customIngress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| superset.customIngress.tls[0].hosts[0] | string | `"superset.{{ .Values.global.ingress.domain }}"` |  |
| superset.enabled | bool | `true` |  |
| superset.extraEnvRaw[0].name | string | `"DB_PASS"` |  |
| superset.extraEnvRaw[0].valueFrom.secretKeyRef.key | string | `"password"` |  |
| superset.extraEnvRaw[0].valueFrom.secretKeyRef.name | string | `"postgres-credentials"` |  |
| superset.extraEnvRaw[1].name | string | `"SUPERSET_SECRET_KEY"` |  |
| superset.extraEnvRaw[1].valueFrom.secretKeyRef.key | string | `"secret-key"` |  |
| superset.extraEnvRaw[1].valueFrom.secretKeyRef.name | string | `"superset-env"` |  |
| superset.extraEnvRaw[2].name | string | `"RELEASE_NAME"` |  |
| superset.extraEnvRaw[2].valueFrom.fieldRef.fieldPath | string | `"metadata.labels['release']"` |  |
| superset.extraVolumeMounts[0].mountPath | string | `"/app/superset_home"` |  |
| superset.extraVolumeMounts[0].name | string | `"{{ .Values.persistence.name }}"` |  |
| superset.extraVolumes[0].name | string | `"{{ .Values.persistence.name }}"` |  |
| superset.extraVolumes[0].persistentVolumeClaim.claimName | string | `"{{ .Values.persistence.name }}"` |  |
| superset.init.loadExamples | bool | `true` |  |
| superset.persistence.enabled | bool | `true` |  |
| superset.persistence.name | string | `"superset-home"` |  |
| superset.persistence.size | string | `"10Gi"` |  |
| superset.postgresql.enabled | bool | `false` |  |
| superset.supersetNode.connections.db_host | string | `"{{ .Release.Name}}-postgresql"` |  |
| superset.supersetNode.connections.db_name | string | `"superset"` |  |
| superset.supersetNode.connections.db_port | string | `"5432"` |  |
| superset.supersetNode.connections.db_user | string | `"admin"` |  |
| tolerations | list | `[]` |  |
| volumeMounts | list | `[]` |  |
| volumes | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
