for file in $(ls ${DB_DIRECTORY}); do
  dbname=$(echo $file | sed "s/.sql//g")
  psql -c "DROP DATABASE IF EXISTS ${dbname} WITH (FORCE)"
  psql -c "CREATE DATABASE ${dbname}"
  psql -d ${dbname} -f ${DB_DIRECTORY}/${file}
  #psql -d ${dbname} < ${DB_DIRECTORY}/${file}
done
