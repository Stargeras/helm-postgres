#!/bin/bash

superseturl="http://{{ .Release.Name }}-superset:{{ .Values.superset.service.port }}"
defaultdbs="$PGUSER postgres superset"

#curl --write-out "%{http_code}" $superseturl/api/v1/security/login -X POST --data '{"password": "{{ .Values.superset.init.adminUser.password }}", "provider": "db", "refresh": true, "username": "{{ .Values.superset.init.adminUser.username }}"}' --header 'Content-Type: application/json' 

until [[ $(curl --write-out "%{http_code}" --silent $superseturl/api/v1/security/login -X POST --data '{"password": "{{ .Values.superset.init.adminUser.password }}", "provider": "db", "refresh": true, "username": "{{ .Values.superset.init.adminUser.username }}"}' --header 'Content-Type: application/json' --output /dev/null) -eq 200 ]]; do
  echo "waiting for superset"
  sleep 5
done

token=$(curl --silent $superseturl/api/v1/security/login -X POST --data '{"password": "{{ .Values.superset.init.adminUser.password }}", "provider": "db", "refresh": true, "username": "{{ .Values.superset.init.adminUser.username }}"}' --header 'Content-Type: application/json' | awk -F : '{print $2}' | awk -F '"' '{print $2}')

echo "token: $token"

csrftoken=$(curl --silent $superseturl/api/v1/security/csrf_token/ --header "Authorization: Bearer $token" -c cookie.txt | awk -F : '{print $2}' | awk -F '"' '{print $2}')

echo "csrfToken: $csrftoken"
cat cookie.txt

add_db() {
  dbname=$1
  curl --silent $superseturl/api/v1/database/ -X POST --header 'Content-Type: application/json' --header "Authorization: Bearer $token" --header "X-CSRFToken: $csrftoken" -b cookie.txt --data @- << EOF
{
  "database_name": "$dbname@$PGHOST.$NAMESPACE.svc.cluster.local",
  "sqlalchemy_uri": "postgresql+psycopg2://$PGUSER:$PGPASSWORD@$PGHOST.$NAMESPACE.svc.cluster.local:$PGPORT/$dbname"
}
EOF
}

for file in $(ls ${DB_DIRECTORY}); do
  add_db $(echo $file | sed "s/.sql//g")
done

for db in $defaultdbs; do
  add_db $db
done
